# README #

## Purpose ##

INIP is a simple ini-like file parser. It is useful for, but not limited to, setting up a scientific simulation's variables.

## How do I get set up? ##


To build the project, you will need cmake.
Issued the following commands to build,

    mkdir build && cd build
    cmake ..
    make all

The static library will be present in the `build/` directory.
Issuing `make install` instead, will put the library and header file in the `bin/` directory of the project.

To build the tests, run `make test` from the `build/` directory and run with `./test` after the build has finished. Make sure all tests have `Passed`.

## Quickstart ##

### Ini Syntax ###

Below you can see an example ini file,

```
#!ini

#comment
[group]
int    = 3
double = 15.8
string = "Hello World!"

[new_group]
double  = 12.1
ints    = [1, 2, 3, 4]
strings = ["Hello", "World"]
```

comment lines start with `#`. Group names are enclosed in `[]` and are followed by key-value entries with `=` defining the relation.

Values can be integers, floating poing numbers, strings, or a list of these types. Strings are surrounded either by single `'` or double `"` quotes.

### Usage ###

The first thing you'd want to do, is to create an `INIFile`. This can be done like this,
```
#!c
INIFile* ini = inip_create();
```
You should make sure you always destruct the object created by the above command using,
```
#!c
inip_destruct(ini);
```

To load an ini file from disk, you use
```
#!c
inip_load(ini, "example.ini");
```
This function will return `false` if it fails.

To check if a group or attribute is present, you can use the following two commands,
```
#!c
bool inip_has_group(const INIFile* ini, const char* group_name);
bool inip_has_attribute(const INIFile* ini, const char* key);
```
where `key` is a string of the form `"group.attribute"`.

To get an attribute from the loaded file, you should use the
```
#!c
$type inip_get_$type(INIFile* ini, const char* key, $type default_value);
```
family of functions. When the function fails to find the group and/or attribute, it will instead return the default value provided. It should be noted that for the `char*` variant, you will have to free the string returned, yourself.

For retrieving lists, the analogous functions are of the form,
```
#!c
bool inip_get_$(type)s(INIFile* ini, const char* key, size_t num_elements, $type* default_values);
```
which return `false` if the attribute was not found. A `num_elements` variables is passed, which tells the function to not read a larger number. When the attribute is found, the list of values, will be written to the `default_values` array.
You can check if an attribute is a list, string, or number, using the functions,
```
#!c
bool inip_is_list(INIFile* ini, const char* key);
bool inip_is_string(INIFile* ini, const char* key);
bool inip_is_num(INIFile* ini, const char* key);
```

** NOTE **: The list family of functions are a bit cumbersome, and I will be trying to improve them in the future.